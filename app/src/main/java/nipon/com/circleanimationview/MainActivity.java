package nipon.com.circleanimationview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity {
    private FrameLayout buttonLayout;
    private TextView text;
    private ProgressBar progressBar;
    private View reveal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonLayout = findViewById(R.id.buttonLayout);
        text = findViewById(R.id.text);
        progressBar = findViewById(R.id.progress_bar);
        reveal = findViewById(R.id.reveal);
    }

    public void load(View view) {
        animateButtonWidth();
        fadeOutTextAndShowProgressBar();
        nextAction();
    }


    private void animateButtonWidth() {
        ValueAnimator anim = ValueAnimator.ofInt(buttonLayout.getMeasuredWidth(), getFinalWidth());

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = buttonLayout.getLayoutParams();
                layoutParams.width = val;
                buttonLayout.requestLayout();
            }
        });

        anim.setDuration(250);
        anim.start();
    }

    private int getFinalWidth() {
        return (int) getResources().getDimension(R.dimen.fabSize);
    }


    private void fadeOutTextAndShowProgressBar() {
        text.animate()
                .alpha(0f)
                .setDuration(250)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        showProgressDialog();
                    }
                })
                .start();
    }

    private void showProgressDialog() {
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        progressBar.setVisibility(VISIBLE);
    }


    private void nextAction() {
        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                revealButton();
                fadeOutProgressBar();
                startNextActivity();

            }
        }, 2000);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void revealButton() {
        buttonLayout.setElevation(0f);

        reveal.setVisibility(VISIBLE);

        int cx = reveal.getWidth();
        int cy = reveal.getHeight();

        int x =  (reveal.getLeft() + reveal.getRight())/2;
        int y = (reveal.getTop() + reveal.getBottom())/2;

        int startX = (int) (getFabWidth() / 2 + buttonLayout.getX());
        int startY = (int) (getFabWidth() / 2 + buttonLayout.getY());

        float finalRadius = Math.max(cx, cy) * 1.2f;

        Animator reveal = ViewAnimationUtils
                .createCircularReveal(findViewById(R.id.reveal), x, y, getFabWidth(), finalRadius);

        reveal.setDuration(500);
        reveal.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                finish();
            }
        });

        reveal.start();
    }

    private int getFabWidth() {
        return (int) getResources().getDimension(R.dimen.fabSize);
    }

    private void fadeOutProgressBar() {
        progressBar.animate().alpha(0f).setDuration(200).start();
    }

    private void startNextActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(MainActivity.this, SecondActivity.class));
                overridePendingTransition(R.anim.stay,R.anim.stay);
            }
        }, 100);
    }

}
